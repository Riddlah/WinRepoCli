unit Unit1;

{$mode objfpc}{$H+}
{$codepage cp1252}
interface

uses
  Classes, SysUtils, LazFileUtils, Process, INIFiles, FileUtil, Forms, Controls,
  Graphics, Dialogs, StdCtrls, ExtCtrls, Menus, CheckLst, ComCtrls, Types,
  Unit2, Unit3, Unit4, FTPSend, Zipper, blcksock, EditBtn, ValEdit, Grids,
  synsock;

type

  { RepoFile}

  GenreObject = Object
  public
     GenreName: String;
     SoftCount: Integer;
  end;

  SoftObject = object
  public
     FileName: String;
     PreviewName: String;
     Name: String;
     About: String;
     Version: String;
     GenreID: Integer;
     Commandline: String;
  end;

  NetRepoFileObject = object
  public
     INIFile: TINIFile;
     SoftCount: Integer;
     GenresCount: Integer;
     Genre: array[1..5000] of GenreObject;
     Soft: array[1..5000] of SoftObject;
     Name: String;
     SoftFolder: String;
     PreviewFolder: String;
  end;

  { TMainForm }

  TMainForm = class(TForm)
    Button_Clone: TButton;
    Button_SelectAll: TButton;
    Button_Update: TButton;
    Button_Install: TButton;
    CheckList_SoftName: TCheckListBox;
    Combo_RepoList: TComboBox;
    Image_Preview: TImage;
    Label_SoftVer: TLabel;
    Label_SoftAbout: TLabel;
    Label_SoftName: TLabel;
    List_SoftGenre: TListBox;
    MainMenu: TMainMenu;
    Menu_RepoEditor: TMenuItem;
    Menu_UpdCheck: TMenuItem;
    Menu_ClearTemp: TMenuItem;
    Menu_About: TMenuItem;
    Menu_Settings: TMenuItem;
    Panel_TypeOfRepo: TPanel;
    ProgressBar: TProgressBar;
    ProgressBar_Download: TProgressBar;
    StatusBar: TStatusBar;
    procedure Button_CloneClick(Sender: TObject);
    procedure Button_InstallClick(Sender: TObject);
    procedure Button_SelectAllClick(Sender: TObject);
    procedure Button_UpdateClick(Sender: TObject);
    procedure CheckList_SoftNameClick(Sender: TObject);
    procedure Combo_RepoListChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure List_SoftGenreClick(Sender: TObject);
    procedure Menu_AboutClick(Sender: TObject);
    procedure Menu_ClearTempClick(Sender: TObject);
    procedure Menu_RepoEditorClick(Sender: TObject);
    procedure Menu_SettingsClick(Sender: TObject);
    procedure Menu_UpdCheckClick(Sender: TObject);
    procedure OnCreate(Sender: TObject);
    procedure ProgressBarContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);

  private
  public

  end;



type
   { CloneRepoThread }

  CloneRepoThread=class(TThread)
private
  result,progressmax,progress,progressdownloadmax,progressdownload:integer;
  FTPClient3:TFTPSend;
  procedure OnMonitor(Sender: TObject; Writing: Boolean;
            const Buffer: TMemory; Len: Integer);
protected
  procedure ShowResult;
  procedure Execute; override;
end;

   { DownAndInstThread }

  DownAndInstThread=class(TThread)
private
  result,progressmax,progress,progressdownloadmax,progressdownload:integer;
  FTPClient2:TFTPSend;
  procedure OnMonitor(Sender: TObject; Writing: Boolean;
            const Buffer: TMemory; Len: Integer);
protected
  procedure ShowResult;
  procedure Execute; override;
end;

  { CliUpdate }

  CliUpdate=class(TThread)
private
  result:integer;
  procedure OnMonitor(Sender: TObject; Writing: Boolean;
            const Buffer: TMemory; Len: Integer);
protected
  procedure ShowResult;
  procedure Execute; override;
end;

{ CacheUpdate }

  CacheUpdate=class(TThread)
private
    result,DownloadMax,LoadMax, Downloaded, DownloadedMax: integer;
    ResetProgressBar: Boolean;
    procedure OnMonitor(Sender: TObject; Writing: Boolean;
              const Buffer: TMemory; Len: Integer);
protected
    Procedure ShowResult;
    Procedure Execute; override;
end;
var
  MainForm: TMainForm;
  ConfigFile, LangFile, RepoFile: TINIFile;
  FTPClient,FTPClient_UPDCHK: TFTPSend;
  Soft_On_Genre_Count: array[1..100] of integer;
  SoftID_Checked: array[0..65536] of boolean;
  Selected_Soft_ID, Selected_Genre_ID, Selected_Repo_ID, SoftCount, GenreCount, RepoCount, PrevGenresSoftCount: Integer;
  Proc_output, TempFolder:string;
  OnlyLocal:Boolean;
  NetRepoFiles: array[1..1000] of NetRepoFileObject;
  CheckedFileID: TObject;
implementation

{$R *.lfm}

{ CloneRepoThread }

procedure CloneRepoThread.OnMonitor(Sender: TObject; Writing: Boolean;
  const Buffer: TMemory; Len: Integer);
begin
     mainform.progressbar_download.Position:=mainform.progressbar_download.Position+Len;
     MainForm.StatusBar.Panels[1].Text:=IntToStr(MainForm.Progressbar_Download.Position)+'/'+IntToStr(MainForm.Progressbar_Download.Max);
     sleep(10);
end;

procedure CloneRepoThread.ShowResult;
begin
     MainForm.ProgressBar.Max:=progressmax;
     MainForm.ProgressBar.Position:=progress;
     MainForm.ProgressBar_Download.Max:=progressdownloadmax;
     MainForm.ProgressBar_Download.Position:=progressdownload;
     case result of
     0:
       begin
            MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','panel_status_downloading','ERROR');
       end;
     1:
       begin
            MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','panel_status_installing','ERROR');
       end;
     2:
       begin
            MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','panel_status_done','ERROR');
            MainForm.Menu_UpdCheck.Enabled:=True;
            MainForm.Menu_Settings.Enabled:=True;
            MainForm.Menu_ClearTemp.Enabled:=True;
            MainForm.Menu_RepoEditor.Enabled:=True;
            MainForm.Button_SelectAll.Enabled:=True;
            MainForm.Button_Update.Enabled:=True;
            MainForm.Button_Install.Enabled:=True;
            MainForm.Button_Clone.Enabled:=True;
            MainForm.Combo_RepoList.Enabled:=True;
            MainForm.CheckList_SoftName.Enabled:=True;
            MainForm.List_SoftGenre.Enabled:=True;
            mainform.progressbar_download.Position:=0;
            MainForm.Combo_RepoListChange(Application);
       end;
     3:
       begin
            MainForm.Menu_UpdCheck.Enabled:=False;
            MainForm.Menu_Settings.Enabled:=False;
            MainForm.Menu_ClearTemp.Enabled:=False;
            MainForm.Menu_RepoEditor.Enabled:=False;
            MainForm.Button_SelectAll.Enabled:=False;
            MainForm.Button_Update.Enabled:=False;
            MainForm.Button_Install.Enabled:=False;
            MainForm.Button_Clone.Enabled:=False;
            MainForm.Combo_RepoList.Enabled:=False;
            MainForm.CheckList_SoftName.Enabled:=False;
            MainForm.List_SoftGenre.Enabled:=False;
       end;
     end;
end;

procedure CloneRepoThread.Execute;
var
   i, softlist_checked_count:integer;
begin
     result:=3;
     progress:=0;
     progressmax:=1;
     progressdownload:=0;
     progressdownloadmax:=1;
     Synchronize(@ShowResult);
     softlist_checked_count:=0;
 //Downloading
          if not(DirectoryExistsUTF8(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder)) then ForceDirectoriesUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder);

          for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do begin
               if SoftID_Checked[i] then softlist_checked_count:=softlist_checked_count+1;
          end;

          Result:=0;
          Synchronize(@ShowResult);
          sleep(10);
          FTPClient3:=TFTPSend.Create;

          FTPClient3.DSock.OnMonitor:=@OnMonitor;

          FTPClient3.TargetHost:=RepoFile.ReadString('Host',inttostr(Selected_Repo_ID),'127.0.0.1');
          FTPClient3.TargetPort:=RepoFile.ReadString('Port',inttostr(Selected_Repo_ID),'21');
          FTPClient3.UserName:=RepoFile.ReadString('UserName',inttostr(Selected_Repo_ID),'Anonymous');
          FTPClient3.Password:=RepoFile.ReadString('Password',inttostr(Selected_Repo_ID),'');


          if FTPClient3.Login then
          begin
               progress:=0;
               progressdownload:=0;
               progressmax:=NetRepoFiles[Selected_Repo_ID].SoftCount;
               Synchronize(@ShowResult);
               for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do
               begin
                         if not(DirectoryExistsUTF8(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder)) then ForceDirectoriesUTF8(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder);
                         if not(FileExists((RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName)))then
                         begin
                              progressdownloadmax:=FTPClient3.FileSize(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName);
                              Synchronize(@ShowResult);
                              FTPClient3.RetrieveFile(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName,FALSE);
                              FTPClient3.DataStream.SaveToFile(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName);
                              progressdownload:=0;
                         end;
                         progress:=progress+1;
                         Synchronize(@ShowResult);
               end;
          end else showmessage(LangFile.ReadString('Translation','Error_FTP','FTP ERROR: LANGFILE ERROR'));
          FTPClient3.RetrieveFile(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+RepoFile.ReadString('Path',inttostr(Selected_Repo_ID),''),FALSE);
          FTPClient3.DataStream.SaveToFile(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+RepoFile.ReadString('Path',inttostr(Selected_Repo_ID),''));
          FTPClient3.Free;
          RepoFile.WriteString('IsLocal',IntToStr(Selected_Repo_ID), 'Yes');
          Result:=1;
          sleep(10);
          progress:=0;
          Synchronize(@ShowResult);
     Result:=2;
     Synchronize(@ShowResult);
end;

{ CacheUpdate }

procedure CacheUpdate.ShowResult;
var
   LangFileString,RepoFileString:string;
   i,j,n:integer;
begin
     case result of
     0: begin
             MainForm.Menu_UpdCheck.Enabled:=False;
             MainForm.Menu_Settings.Enabled:=False;
             MainForm.Menu_ClearTemp.Enabled:=False;
             MainForm.Menu_RepoEditor.Enabled:=False;
             MainForm.Button_SelectAll.Enabled:=False;
             MainForm.Button_Update.Enabled:=False;
             MainForm.Button_Install.Enabled:=False;
             MainForm.Button_Clone.Enabled:=False;
             MainForm.Combo_RepoList.Enabled:=False;
             MainForm.CheckList_SoftName.Enabled:=False;
             MainForm.List_SoftGenre.Enabled:=False;
             MainForm.Enabled:=False;
             MainForm.StatusBar.Panels[0].Text:='Caching';
        end;

     1: begin
             MainForm.ProgressBar_Download.Position:=0;
             MainForm.ProgressBar_Download.Max:=DownloadMax;
             //MainForm.ProgressBar.Position:=0;
             MainForm.ProgressBar.Max:=DownloadedMax;

        end;
     2: begin
          MainForm.ProgressBar.Position:=0;
          n:=0;
          if (FileExists('config.ini')) then
          begin
               ConfigFile:=TINIFile.Create('config.ini');

               MainForm.Combo_RepoList.Clear;

               MainForm.Button_Install.Font.Size:=ConfigFile.ReadInteger('TextSize','Button_Install',5);
               MainForm.Label_SoftName.Font.Size:=ConfigFile.ReadInteger('TextSize','Label_SoftName',5);
               MainForm.Label_SoftAbout.Font.Size:=ConfigFile.ReadInteger('TextSize','Label_SoftAbout',5);
               MainForm.Label_SoftVer.Font.Size:=ConfigFile.ReadInteger('TextSize','Label_SoftVer',5);
               MainForm.List_SoftGenre.Font.Size:=ConfigFile.ReadInteger('TextSize','List_SoftRepo',5);
               MainForm.CheckList_SoftName.Font.Size:=ConfigFile.ReadInteger('TextSize','List_SoftName',5);

               TempFolder:=ConfigFile.ReadString('Paths','Tempfolder','temp/');

               LangFileString:=ConfigFile.ReadString('Paths','LangFile','');
               If(FileExists(LangFileString)) then
               begin
                    LangFile:=TINIFile.Create(LangFileString);

                    MainForm.Caption:=LangFile.ReadString('Translation','MainForm','LANGFILE ERROR');

                    MainForm.Menu_Settings.Caption:=LangFile.ReadString('Translation','Menu_Settings','LANGFILE ERROR');
                    MainForm.Menu_About.Caption:=LangFile.ReadString('Translation','Label_About','Why did you kill me?');
                    MainForm.Menu_ClearTemp.Caption:=LangFile.ReadString('Translation','Menu_ClearTemp','');
                    MainForm.Menu_UpdCheck.Caption:=LangFile.ReadString('Translation','Menu_UpdCheck','');
                    MainForm.Menu_RepoEditor.Caption:=LangFile.ReadString('Translation','Menu_RepoEditor','');

                    MainForm.Button_Install.Caption:=LangFile.ReadString('Translation','Button_Install','LANGFILE ERROR');
                    MainForm.Button_Update.Caption:=LangFile.ReadString('Translation','Button_Update','LANGFILE ERROR');
                    MainForm.Button_SelectAll.Caption:=LangFile.ReadString('Translation','Button_SelectAll','LANGFILE ERROR');
                    MainForm.Button_Clone.Caption:=LangFile.ReadString('Translation','Button_Clone','LANGFILE ERROR');

                    MainForm.Label_SoftName.Caption:=LangFile.ReadString('Translation','Label_SoftName','LANGFILE ERROR');
                    MainForm.Label_SoftAbout.Caption:=LangFile.ReadString('Translation','Label_SoftAbout','LANGFILE ERROR');
                    MainForm.Label_SoftVer.Caption:=LangFile.ReadString('Translation','Label_SoftVer','LANGFILE ERROR');
                    //Label_Repository_Server.Caption:=LangFile.ReadString('Translation','Label_Repository_Server','LANGFILE ERROR');

               end else showmessage('Lang File not found!');

               RepoFileString:=ConfigFile.ReadString('Paths','RepoFile','');
               if(FileExists(RepoFileString)) then
               begin
                    RepoFile:=TINIFile.Create(RepoFileString);
                    RepoCount:=RepoFile.ReadInteger('General','RepoCount',1);
                    for i:=1 to RepoCount do
                    begin
                         if RepoFile.ReadString('IsLocal',inttostr(i),'Yes')='Yes' then
                         begin
                              if FileExists(RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),'')) then
                              begin
                                   NetRepoFiles[i].INIFile:=TINIFile.Create(RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),''));
                                   NetRepoFiles[i].Name:=NetRepoFiles[i].INIFile.ReadString('General','Name','Unknown');
                                   NetRepoFiles[i].SoftCount:=NetRepoFiles[i].INIFile.ReadInteger('General','SoftCount',0);
                                   NetRepoFiles[i].GenresCount:=NetRepoFiles[i].INIFile.ReadInteger('General','GenresCount',0);
                                   NetRepoFiles[i].SoftFolder:=NetRepoFiles[i].INIFile.ReadString('General','SoftFolder','');
                                   NetRepoFiles[i].PreviewFolder:=NetRepoFiles[i].INIFile.ReadString('General','PreviewFolder','');

                                   for j:=1 to NetRepoFiles[i].SoftCount do
                                   begin
                                        NetRepoFiles[i].Soft[j].FileName:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'path','');
                                        NetRepoFiles[i].Soft[j].PreviewName:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'preview','');
                                        NetRepoFiles[i].Soft[j].Name:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'name','Unknown');
                                        NetRepoFiles[i].Soft[j].About:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'about','Unknown');
                                        NetRepoFiles[i].Soft[j].Version:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'ver','Unknown');
                                        NetRepoFiles[i].Soft[j].GenreID:=NetRepoFiles[i].INIFile.ReadInteger(inttostr(j),'Genre',1);
                                        NetRepoFiles[i].Soft[j].Commandline:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'commandline','');
                                   end;

                                   for j:=1 to NetRepoFiles[i].GenresCount do
                                   begin
                                        NetRepoFiles[i].Genre[j].GenreName:=NetRepoFiles[i].INIFile.ReadString('GenresNames',inttostr(j),'Unknown');
                                        NetRepoFiles[i].Genre[j].SoftCount:=0;
                                        for n:=1 to NetRepoFiles[i].SoftCount do
                                        begin
                                             if NetRepoFiles[i].Soft[n].GenreID=j then
                                             begin
                                                  NetRepoFiles[i].Genre[j].SoftCount:=NetRepoFiles[i].Genre[j].SoftCount+1;
                                             end;
                                        end;
                                   end;


                                   MainForm.Combo_RepoList.Items.Add(NetRepoFiles[i].Name+' '+'('+IntToStr(NetRepoFiles[i].SoftCount)+')');
                              end else begin
                                  showmessage('Missing file path found!');
                                  NetRepoFiles[i].INIFile:=TINIFile.Create('config.ini');
                                  MainForm.List_SoftGenre.Items.Add('Unknown');
                              end;

                         end else
                         begin
                              if FileExists(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),'')) then
                              begin
                                   NetRepoFiles[i].INIFile:=TINIFile.Create(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path', inttostr(i),''));
                                   NetRepoFiles[i].Name:=NetRepoFiles[i].INIFile.ReadString('General','Name','Unknown');
                                   NetRepoFiles[i].SoftCount:=NetRepoFiles[i].INIFile.ReadInteger('General','SoftCount',0);
                                   NetRepoFiles[i].GenresCount:=NetRepoFiles[i].INIFile.ReadInteger('General','GenresCount',0);
                                   NetRepoFiles[i].SoftFolder:=NetRepoFiles[i].INIFile.ReadString('General','SoftFolder','');
                                   NetRepoFiles[i].PreviewFolder:=NetRepoFiles[i].INIFile.ReadString('General','PreviewFolder','');

                                   for j:=1 to NetRepoFiles[i].SoftCount do
                                   begin
                                        NetRepoFiles[i].Soft[j].FileName:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'path','');
                                        NetRepoFiles[i].Soft[j].PreviewName:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'preview','');
                                        NetRepoFiles[i].Soft[j].Name:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'name','Unknown');
                                        NetRepoFiles[i].Soft[j].About:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'about','Unknown');
                                        NetRepoFiles[i].Soft[j].Version:=NetRepoFiles[i].INIFile.ReadString(inttostr(j),'ver','Unknown');
                                        NetRepoFiles[i].Soft[j].GenreID:=NetRepoFiles[i].INIFile.ReadInteger(inttostr(j),'genre',1);
                                   end;

                                   for j:=1 to NetRepoFiles[i].GenresCount do
                                   begin
                                        NetRepoFiles[i].Genre[j].GenreName:=NetRepoFiles[i].INIFile.ReadString('GenresNames',inttostr(j),'Unknown');
                                        NetRepoFiles[i].Genre[j].SoftCount:=0;
                                        for n:=1 to NetRepoFiles[i].SoftCount do
                                        begin
                                             if NetRepoFiles[i].Soft[n].GenreID=j then
                                             begin
                                                  NetRepoFiles[i].Genre[j].SoftCount:=NetRepoFiles[i].Genre[j].SoftCount+1;
                                             end;
                                        end;
                                   end;

                                   MainForm.Combo_RepoList.Items.Add(NetRepoFiles[i].Name+' ('+IntToStr(NetRepoFiles[i].SoftCount)+')')

                              end;
                         end;
                    end;
               end else showmessage('Repo File not found');
               MainForm.Combo_RepoList.ItemIndex:=0;
               Selected_Repo_ID:=0;
               MainForm.Combo_RepoListChange(Application);

          end else showmessage('Config.ini not found! Please put config in root folder of this software');

             MainForm.Menu_UpdCheck.Enabled:=True;
             MainForm.Menu_Settings.Enabled:=True;
             MainForm.Menu_ClearTemp.Enabled:=True;
             MainForm.Menu_RepoEditor.Enabled:=True;
             MainForm.Button_SelectAll.Enabled:=True;
             MainForm.Button_Update.Enabled:=True;
             MainForm.Button_Install.Enabled:=True;
             MainForm.Button_Clone.Enabled:=True;
             MainForm.Combo_RepoList.Enabled:=True;
             MainForm.CheckList_SoftName.Enabled:=True;
             MainForm.List_SoftGenre.Enabled:=True;
             Mainform.progressbar_download.Position:=0;
             MainForm.Enabled:=True;
             MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','Panel_Status_Done','LangFileError');
        end;
     end;
end;

procedure CacheUpdate.OnMonitor(Sender: TObject; Writing: Boolean;
  const Buffer: TMemory; Len: Integer);
begin
  mainform.progressbar_download.Position:=mainform.progressbar_download.Position+Len;
  MainForm.StatusBar.Panels[1].Text:=IntToStr(MainForm.Progressbar_Download.Position)+'/'+IntToStr(MainForm.Progressbar_Download.Max);
  MainForm.ProgressBar.Position:=Downloaded;
  sleep(10);
end;

procedure CacheUpdate.Execute;
var
   FTPClient: TFTPSend;
   ConfigFile, LangFile, RepoFile: TINIFile;
   SoftFile: array[1..100] of TINIFile;
   TempFolder,LangFileString,RepoFileString: String;
   RepoCount,i,j:Integer;
   f1:textfile;
begin
     result:=0;
     Downloaded:=0;
     DownloadedMax:=0;
     ResetProgressbar:=False;
     Synchronize(@ShowResult);
     if FileExists('config.ini') then
     begin
          ConfigFile:=TINIFile.Create('config.ini');
          TempFolder:=ConfigFile.ReadString('Paths','Tempfolder','temp/');
          LangFileString:=ConfigFile.ReadString('Paths','LangFile','');

          If(FileExists(LangFileString)) then
          begin
               LangFile:=TINIFile.Create(LangFileString);
          end else showmessage('LANG FILE NOT FOUND!');

          RepoFileString:=ConfigFile.ReadString('Paths','RepoFile','');
          if(FileExists(RepoFileString)) then
          begin
               RepoFile:=TINIFile.Create(RepoFileString);
               RepoCount:=RepoFile.ReadInteger('General','RepoCount',1);
          end else showmessage('REPOFILE NOT FOUND!');


          for i:=1 to RepoCount do
          begin
               if RepoFile.ReadString('IsLocal',inttostr(i),'Yes')='No' then
               begin
                    if not(FileExists(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),''))) then
                    begin
                         if not(DirectoryExistsUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),''))) then ForceDirectoriesUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),''));
                         FTPClient:=TFTPSend.Create;
                         FTPClient.DSock.OnMonitor:=@OnMonitor;
                         FTPClient.TargetHost:=RepoFile.ReadString('Host',inttostr(i),'127.0.0.1');
                         FTPClient.TargetPort:=RepoFile.ReadString('Port',inttostr(i),'21');
                         FTPClient.UserName:=RepoFile.ReadString('UserName',inttostr(i),'Anonymous');
                         FTPClient.Password:=RepoFile.ReadString('Password',inttostr(i),'');
                         if FTPClient.Login then
                         begin
                              DownloadMax:=FTPClient.FileSize(RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),''));
                              Result:=1;
                              Downloaded:=0;
                              Synchronize(@ShowResult);
                              FTPClient.RetrieveFile(RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),''),FALSE);
                              FTPClient.DataStream.SaveToFile(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),''));
                              SoftFile[i]:=TINIFile.Create(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path', inttostr(i),''));
                              DownloadedMax:=SoftFile[i].ReadInteger('General','SoftCount',0);
                              Synchronize(@ShowResult);
                              for j:=1 to SoftFile[i].ReadInteger('General','SoftCount',0) do
                              begin
                                   if not(FileExists(TempFolder+SoftFile[i].ReadString('General','previewfolder','')+SoftFile[i].ReadString(inttostr(j),'preview',''))) then
                                   begin
                                        if not(DirectoryExistsUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+SoftFile[i].ReadString('General','previewfolder',''))) then ForceDirectoriesUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+SoftFile[i].ReadString('General','previewfolder',''));
                                        DownloadMax:=FTPClient.FileSize(RepoFile.ReadString('Folder',inttostr(i),'')+'/'+SoftFile[i].ReadString('General','previewfolder','')+'/'+SoftFile[i].ReadString(inttostr(j),'preview',''));
                                        Synchronize(@ShowResult);
                                        FTPClient.RetrieveFile(RepoFile.ReadString('Folder',inttostr(i),'')+'/'+SoftFile[i].ReadString('General','previewfolder','')+'/'+SoftFile[i].ReadString(inttostr(j),'preview',''),FALSE);
                                        FTPClient.DataStream.SaveToFile(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+SoftFile[i].ReadString('General','previewfolder','')+'/'+SoftFile[i].ReadString(inttostr(j),'preview',''));
                                        Downloaded:=Downloaded+1;
                                        //showmessage(TempFolder+'/'+SoftFile[i].ReadString(inttostr(j),'preview',''));
                                   end;
                              end;
                         end else begin
                              ShowMessage(LangFile.ReadString('Translation','Error_FTP','FTP ERROR: LANGFILE ERROR'));
                              AssignFile(f1,TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(i),'')+'/'+RepoFile.ReadString('Path',inttostr(i),''));
                              rewrite(f1);
                              closefile(f1);
                         end;

                    end;
               end;
          end;
     end else showmessage('CONFIG NOT FOUND!');
     //FTPClient.Free();
     Result:=2;
     Synchronize(@ShowResult);
end;





{ CliUpdate }

procedure CliUpdate.OnMonitor(Sender: TObject; Writing: Boolean;
  const Buffer: TMemory; Len: Integer);
begin
  mainform.progressbar_download.Position:=mainform.progressbar_download.Position+Len;
  sleep(10);
end;

procedure CliUpdate.ShowResult;
begin
     if result=1 then
     begin
          MainForm.Menu_UpdCheck.Enabled:=True;
          MainForm.Menu_Settings.Enabled:=True;
          MainForm.Menu_ClearTemp.Enabled:=True;
          MainForm.Menu_RepoEditor.Enabled:=True;
          MainForm.Button_SelectAll.Enabled:=True;
          MainForm.Button_Update.Enabled:=True;
          MainForm.Button_Install.Enabled:=True;
          MainForm.Combo_RepoList.Enabled:=True;
          MainForm.CheckList_SoftName.Enabled:=True;
          MainForm.List_SoftGenre.Enabled:=True;
          mainform.progressbar_download.Position:=0;
     end else
     begin
          MainForm.Menu_UpdCheck.Enabled:=False;
          MainForm.Menu_Settings.Enabled:=False;
          MainForm.Menu_ClearTemp.Enabled:=False;
          MainForm.Menu_RepoEditor.Enabled:=False;
          MainForm.Button_SelectAll.Enabled:=False;
          MainForm.Button_Update.Enabled:=False;
          MainForm.Button_Install.Enabled:=False;
          MainForm.Combo_RepoList.Enabled:=False;
          MainForm.CheckList_SoftName.Enabled:=False;
          MainForm.List_SoftGenre.Enabled:=False;
     end;
end;

procedure CliUpdate.Execute;
var
   VerInfoFile, ConfigFile, LangFile: TINIFile;
   UnZip:TUnZipper;
   CurVer, NewVer:Integer;
   UpdateDialog: integer;
begin
     result:=0;
     Synchronize(@ShowResult);
     ConfigFile:=TINIFile.Create('config.ini');
     LangFile:=TINIFile.Create(ConfigFile.ReadString('Paths','LangFile',''));
     CurVer:=ConfigFile.ReadInteger('Updates','ver',1);
     FTPClient_UPDCHK:=TFTPSend.Create;
     FTPClient_UPDCHK.DSock.OnMonitor:=@OnMonitor;
     FTPClient_UPDCHK.TargetHost:=ConfigFile.ReadString('Updates','Host','127.0.0.1');
     FTPClient_UPDCHK.TargetPort:=ConfigFile.ReadString('Updates','Port','21');
     FTPClient_UPDCHK.UserName:=ConfigFile.ReadString('Updates','UserName','Anonymous');
     FTPClient_UPDCHK.Password:=ConfigFile.ReadString('Updates','Pass','');
     if FTPClient_UPDCHK.Login then
     begin
        if not(DirectoryExistsUTF8(TempFolder+'/'+ConfigFile.ReadString('Updates','Folder',''))) then ForceDirectoriesUTF8(TempFolder+'/'+ConfigFile.ReadString('Updates','Folder',''));
        if FileExistsUTF8(TempFolder+'/'+ConfigFile.ReadString('Updates','Folder','')+'/'+ConfigFile.ReadString('Updates','VerInfoFile','verinfo.ini')) then DeleteFile(TempFolder+'/'+ConfigFile.ReadString('Updates','Folder','')+'/'+ConfigFile.ReadString('Updates','VerInfoFile','verinfo.ini'));
        FTPClient_UPDCHK.RetrieveFile(ConfigFile.ReadString('Updates','Folder','')+'/'+ConfigFile.ReadString('Updates','VerInfoFile','verinfo.ini'),FALSE);
        FTPClient_UPDCHK.DataStream.SaveToFile(TempFolder+'/'+ConfigFile.ReadString('Updates','Folder','')+'/'+ConfigFile.ReadString('Updates','VerInfoFile','verinfo.ini'));

        VerInfoFile:=TINIFile.Create(TempFolder+'/'+ConfigFile.ReadString('Updates','Folder','')+'/'+ConfigFile.ReadString('Updates','VerInfoFile','verinfo.ini'));
        NewVer:=VerInfoFile.ReadInteger('Info','Ver',0);

        if NewVer>CurVer then
        begin
             UpdateDialog:=MessageDlg(LangFile.ReadString('Translation','Update_Dialog_Text','?')+IntToStr(NewVer),mtConfirmation,mbOKCancel,0);
             if UpdateDialog = mrOK then
             begin
                  if FileExists(ConfigFile.ReadString('Updates','CurExe','winrepocli.exe')) then
                  begin
                       if FileExistsUTF8('backup.exe') then DeleteFileUTF8('backup.exe');
                       RenameFile(ConfigFile.ReadString('Updates','CurExe','winrepocli.exe'),'backup.exe');
                  end;
                  if FileExistsUTF8(VerInfoFile.ReadString('Info','zipfile','update.zip')) then DeleteFileUTF8(VerInfoFile.ReadString('Info','zipfile','update.zip'));
                  FTPClient_UPDCHK.RetrieveFile(ConfigFile.ReadString('Updates','Folder','')+'/'+VerInfoFile.ReadString('Info','zipfile','update.zip'),FALSE);
                  FTPClient_UPDCHK.DataStream.SaveToFile(VerInfoFile.ReadString('Info','zipfile','update.zip'));
                  UnZip:=TUnZipper.Create;
                  UnZip.FileName:=VerInfoFile.ReadString('Info','zipfile','update.zip');
                  UnZip.OutputPath:='';
                  UnZip.Examine;
                  UnZip.UnZipAllFiles;
                  UnZip.Free;
                  ConfigFile.WriteInteger('Updates','Ver',NewVer);
                  ShowMessage(LangFile.ReadString('Translation','Settings_NeedRestart_Message',''));

             end;
        end else showmessage(LangFile.ReadString('Translation','Update_NotFound','?')+IntToStr(CurVer));
     end else showmessage(LangFile.ReadString('Translation','Error_FTP','FTP ERROR: LANGFILE ERROR'));
     FTPClient_UPDCHK.Free;
     result:=1;
     Synchronize(@ShowResult);
end;





{ DownAndInstThread }

procedure DownAndInstThread.OnMonitor(Sender: TObject; Writing: Boolean;
  const Buffer: TMemory; Len: Integer);
begin
  mainform.progressbar_download.Position:=mainform.progressbar_download.Position+Len;
  MainForm.StatusBar.Panels[1].Text:=IntToStr(MainForm.Progressbar_Download.Position)+'/'+IntToStr(MainForm.Progressbar_Download.Max);
  sleep(10);
end;

procedure DownAndInstThread.ShowResult;
begin
     MainForm.ProgressBar.Max:=progressmax;
     MainForm.ProgressBar.Position:=progress;
     MainForm.ProgressBar_Download.Max:=progressdownloadmax;
     MainForm.ProgressBar_Download.Position:=progressdownload;
     case result of
     0:
       begin
            MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','panel_status_downloading','ERROR');
       end;
     1:
       begin
            MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','panel_status_installing','ERROR');
       end;
     2:
       begin
            MainForm.StatusBar.Panels[0].Text:=LangFile.ReadString('Translation','panel_status_done','ERROR');
            MainForm.Menu_UpdCheck.Enabled:=True;
            MainForm.Menu_Settings.Enabled:=True;
            MainForm.Menu_ClearTemp.Enabled:=True;
            MainForm.Menu_RepoEditor.Enabled:=True;
            MainForm.Button_SelectAll.Enabled:=True;
            MainForm.Button_Update.Enabled:=True;
            MainForm.Button_Install.Enabled:=True;
            MainForm.Combo_RepoList.Enabled:=True;
            MainForm.CheckList_SoftName.Enabled:=True;
            MainForm.List_SoftGenre.Enabled:=True;
            mainform.progressbar_download.Position:=0;
       end;
     3:
       begin
            MainForm.Menu_UpdCheck.Enabled:=False;
            MainForm.Menu_Settings.Enabled:=False;
            MainForm.Menu_ClearTemp.Enabled:=False;
            MainForm.Menu_RepoEditor.Enabled:=False;
            MainForm.Button_SelectAll.Enabled:=False;
            MainForm.Button_Update.Enabled:=False;
            MainForm.Button_Install.Enabled:=False;
            MainForm.Combo_RepoList.Enabled:=False;
            MainForm.CheckList_SoftName.Enabled:=False;
            MainForm.List_SoftGenre.Enabled:=False;
       end;
     end;
end;

procedure DownAndInstThread.Execute;

var
   i, softlist_checked_count:integer;
   UpdateCheck:CliUpdate;
begin
     result:=3;
     progress:=0;
     progressmax:=1;
     progressdownload:=0;
     progressdownloadmax:=1;
     Synchronize(@ShowResult);
     softlist_checked_count:=0;
     if  RepoFile.ReadString('IsLocal',inttostr(Selected_Repo_ID),'Yes')='Yes' then
     begin
          for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do
          begin
               if SoftID_Checked[i] then softlist_checked_count:=softlist_checked_count+1;
          end;
          Result:=1;
          progressmax:=NetRepoFiles[Selected_Repo_ID].SoftCount-1;
          for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do
          begin
               if SoftID_Checked[i] then
               begin
                    if FileExists(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName) then
                    begin
                         RunCommand(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName,[NetRepoFiles[Selected_Repo_ID].Soft[i+1].Commandline],proc_output);
                    end else showmessage(LangFile.ReadString('Translation','SoftFileError1_message',''));
               end;
               sleep(10);
               progress:=progress+1;
               Synchronize(@ShowResult);
          end;
          Result:=2;
          Synchronize(@ShowResult);

     end else
     begin
          if not(DirectoryExistsUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder)) then ForceDirectoriesUTF8(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder);

          for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do begin
               if SoftID_Checked[i] then softlist_checked_count:=softlist_checked_count+1;
          end;

          Result:=0;
          Synchronize(@ShowResult);
          sleep(10);
          FTPClient2:=TFTPSend.Create;

          FTPClient2.DSock.OnMonitor:=@OnMonitor;

          FTPClient2.TargetHost:=RepoFile.ReadString('Host',inttostr(Selected_Repo_ID),'127.0.0.1');
          FTPClient2.TargetPort:=RepoFile.ReadString('Port',inttostr(Selected_Repo_ID),'21');
          FTPClient2.UserName:=RepoFile.ReadString('UserName',inttostr(Selected_Repo_ID),'Anonymous');
          FTPClient2.Password:=RepoFile.ReadString('Password',inttostr(Selected_Repo_ID),'');


          if FTPClient2.Login then
          begin
               progress:=0;
               progressdownload:=0;
               progressmax:=softlist_checked_count;
               Synchronize(@ShowResult);
               for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do
               begin
                    if SoftID_Checked[i] then
                    begin
                         if not(FileExists((TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName)))then
                         begin
                              progressdownloadmax:=FTPClient2.FileSize(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName);
                              Synchronize(@ShowResult);
                              FTPClient2.RetrieveFile(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName,FALSE);
                              FTPClient2.DataStream.SaveToFile(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName);
                              progressdownload:=0;
                         end;
                         progress:=progress+1;
                         Synchronize(@ShowResult);
                    end;
               end;
          end else showmessage(LangFile.ReadString('Translation','Error_FTP','FTP ERROR: LANGFILE ERROR'));
          FTPClient2.Free;
          Result:=1;
          sleep(10);
          progress:=0;
          Synchronize(@ShowResult);
          for i:=0 to NetRepoFiles[Selected_Repo_ID].SoftCount-1 do
          begin
              if SoftID_Checked[i] then
              begin
                   if FileExists(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName) then
                   begin
                        RunCommand(TempFolder+'/'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'/'+NetRepoFiles[Selected_Repo_ID].SoftFolder+'/'+NetRepoFiles[Selected_Repo_ID].Soft[i+1].FileName,[NetRepoFiles[Selected_Repo_ID].Soft[i+1].Commandline],proc_output);
                   end;
                   progress:=progress+1;
                   Synchronize(@ShowResult);
              end;
          end;
     end;
     Result:=2;
     Synchronize(@ShowResult);
end;



{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
var
   Cache_Download: CacheUpdate;
begin
     //Cache_Download;
     MainForm.Enabled:=False;
     Cache_Download:=CacheUpdate.Create(False);
     Cache_Download.Priority:=tpNormal;
end;

procedure TMainForm.Button_InstallClick(Sender: TObject);
var
   i:integer;
   buf:Integer;
   DownloadAndInstall:DownAndInstThread;
begin

     for i:=0 to CheckList_SoftName.Items.Count-1 do
     begin
          SoftID_Checked[i]:=False;
     end;

     for i:=0 to CheckList_SoftName.Items.Count-1 do
     begin
          if CheckList_SoftName.Checked[i] then
          begin
               buf:=Integer(CheckList_SoftName.Items.Objects[i]);
               SoftID_Checked[buf]:=True;
          end;
     end;
     DownloadAndInstall:=DownAndInstThread.Create(False);
     DownloadAndInstall.Priority:=tpNormal;
end;

procedure TMainForm.Button_CloneClick(Sender: TObject);
var
   CloneRepo:CloneRepoThread;
begin
   CloneRepo:=CloneRepoThread.Create(False);
end;

procedure TMainForm.Button_SelectAllClick(Sender: TObject);
var
     i, buf:integer;
begin
  if CheckList_SoftName.Items.Count>0 then
  begin
       for i:=0 to CheckList_SoftName.Items.Count-1 do
       begin
           CheckList_SoftName.Checked[i]:=True;
           buf:=Integer(CheckList_SoftName.Items.Objects[i]);
           SoftID_Checked[buf]:=True;
       end;
       Button_Install.Enabled:=True;
  end;
end;

procedure TMainForm.Button_UpdateClick(Sender: TObject);
var
   Cache_Download: CacheUpdate;
begin
     List_SoftGenre.Clear;
     CheckList_SoftName.Clear;
     Combo_RepoList.Clear;

     MainForm.Enabled:=False;
     Cache_Download:=CacheUpdate.Create(False);
     //MainForm.Hide;
     //MainForm.Show;
end;

procedure TMainForm.CheckList_SoftNameClick(Sender: TObject);
var Preview:TPicture;
    i,j,buf:integer;
begin
     j:=0;
     if Checklist_SoftName.SelCount>0 then
     begin
          Selected_Soft_ID:=integer(CheckList_SoftName.Items.Objects[CheckList_SoftName.ItemIndex])+1;

               for i:=0 to CheckList_SoftName.Items.Count-1 do
               begin
                    if CheckList_SoftName.Checked[i] then
                    begin
                         buf:=Integer(CheckList_SoftName.Items.Objects[i]);
                         SoftID_Checked[buf]:=True;
                    end else
                    begin
                        buf:=Integer(CheckList_SoftName.Items.Objects[i]);
                        SoftID_Checked[buf]:=False;
                    end;
               end;

          Preview:=TPicture.Create;
          if RepoFile.ReadString('IsLocal',inttostr(Selected_Repo_ID),'YES')='YES' then
          begin
               if FileExists(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'\'+NetRepoFiles[Selected_Repo_ID].PreviewFolder+'\'+NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].PreviewName) then
               begin
                    Preview.LoadFromFile(RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'\'+NetRepoFiles[Selected_Repo_ID].PreviewFolder+'\'+NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].PreviewName);
               end else Preview.LoadFromFile('pic/error.png');
               Image_Preview.Picture:=Preview;
               Preview.free;
          end else
          begin
               if FileExists(TempFolder+'\'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'\'+NetRepoFiles[Selected_Repo_ID].PreviewFolder+'\'+NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].PreviewName) then
               begin
                    Preview.LoadFromFile(TempFolder+'\'+RepoFile.ReadString('Folder',inttostr(Selected_Repo_ID),'')+'\'+NetRepoFiles[Selected_Repo_ID].PreviewFolder+'\'+NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].PreviewName);
                    Image_Preview.Picture:=Preview;
                    Preview.free;
               end;
          end;

          Label_SoftName.Caption:=NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].Name;

          Label_SoftAbout.Caption:=NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].About;
          Label_SoftVer.Caption:=NetRepoFiles[Selected_Repo_ID].Soft[Selected_Soft_ID].Version;
          for i:=0 to CheckList_SoftName.Items.Count-1 do
          begin
               if CheckList_SoftName.Checked[i] then
               begin
                    j:=j+1;
               end;

          end;
          if j>0 then Button_Install.Enabled:=True else Button_Install.Enabled:=False;
     end;
end;

procedure TMainForm.Combo_RepoListChange(Sender: TObject);
var
     i:integer;
begin
     Selected_Repo_ID:=Combo_RepoList.Items.IndexOf(Combo_RepoList.Caption)+1;
     Selected_Genre_ID:=1;
     CheckList_SoftName.Clear;
     List_SoftGenre.Clear;
     if RepoFile.ReadString('IsLocal',inttostr(Selected_Repo_ID),'Yes')='Yes' then
     begin
          Panel_TypeOfRepo.Caption:=LangFile.ReadString('Translation','Label_RepoType_Local','');
          Panel_TypeOfRepo.Font.Color:=clGreen;
          Button_Clone.Enabled:=False;
     end else
     begin
          Panel_TypeOfRepo.Caption:=LangFile.ReadString('Translation','Label_RepoType_FTP','');
          Panel_TypeOfRepo.Font.Color:=clRed;
          Button_Clone.Enabled:=True;
     end;
     List_SoftGenre.Items.Add(LangFile.ReadString('Translation','AllGenres','LANGFILE ERROR')+' ['+IntToStr(NetRepoFiles[Selected_Repo_ID].SoftCount)+']');

     If List_SoftGenre.SelCount=0 then List_SoftGenre.Selected[0]:=TRUE;

     for i:=1 to NetRepoFiles[Selected_Repo_ID].GenresCount do
     begin
          List_SoftGenre.Items.Add(NetRepoFiles[Selected_Repo_ID].Genre[i].GenreName+' ['+IntToStr(NetRepoFiles[Selected_Repo_ID].Genre[i].SoftCount)+']');
     end;

     for i:=1 to NetRepoFiles[Selected_Repo_ID].SoftCount do
     begin
          CheckList_SoftName.Items.AddObject(NetRepoFiles[Selected_Repo_ID].Soft[i].Name, TObject(Integer(i-1)))
     end;

     for i:=0 to 65536 do
     begin
          SoftID_Checked[i]:=False;
     end;
end;

procedure TMainForm.List_SoftGenreClick(Sender: TObject);
var
   i, Selected_Genre_ID, buf:integer;

begin
     Selected_Genre_ID:=List_SoftGenre.ItemIndex;
     PrevGenresSoftCount:=0;

     CheckList_SoftName.Clear;
     if Selected_Genre_ID=0 then
     begin
          for i:=1 to NetRepoFiles[Selected_Repo_ID].SoftCount do
          begin
               CheckList_SoftName.Items.AddObject(NetRepoFiles[Selected_Repo_ID].Soft[i].Name, TObject(Integer(i-1)));
          end;
     end else
     begin
          for i:=1 to NetRepoFiles[Selected_Repo_ID].SoftCount do
          begin
               if (NetRepoFiles[Selected_Repo_ID].Soft[i].GenreID=Selected_Genre_ID) then
               begin
                    CheckList_SoftName.Items.AddObject(NetRepoFiles[Selected_Repo_ID].Soft[i].Name, TObject(Integer(i-1)));
               end;
          end;
     end;

     for i:=0 to CheckList_SoftName.Items.Count-1 do
     begin
          buf:=Integer(CheckList_SoftName.Items.Objects[i]);
          if SoftID_Checked[buf] then
          begin
               CheckList_SoftName.Checked[i]:=True;
          end;
     end;
end;

procedure TMainForm.Menu_AboutClick(Sender: TObject);
begin
     About.Show;
end;

procedure TMainForm.Menu_ClearTempClick(Sender: TObject);
var
   Cache_Download: CacheUpdate;
begin
     DeleteDirectory(TempFolder,FALSE);
     Cache_Download:=CacheUpdate.Create(False);
     Cache_Download.Priority:=tpNormal;
     //Cache_Download;
     //Statusbar.(LangFile.ReadString('Translation','panel_status_Done','ERROR'));
end;

procedure TMainForm.Menu_RepoEditorClick(Sender: TObject);
begin
     RepoListEditor.Show;
end;

procedure TMainForm.Menu_SettingsClick(Sender: TObject);
begin
     Settings.Show;
end;

procedure TMainForm.Menu_UpdCheckClick(Sender: TObject);
var
   UpdateCheck:CliUpdate;
begin
     UpdateCheck:=CliUpdate.Create(False);
     UpdateCheck.Priority:=tpNormal;
end;

procedure TMainForm.OnCreate(Sender: TObject);
begin
     //Cache_Download;
end;

procedure TMainForm.ProgressBarContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin

end;



end.


