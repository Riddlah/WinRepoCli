unit Unit3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, INIFiles;

type

  { TAbout }

  TAbout = class(TForm)
    Label_ServerParser: TLabel;
    Label_ServerParser_Text: TLabel;
    Label_Year: TLabel;
    Label_Ver: TLabel;
    Label_Author: TLabel;
    Label_Logo: TLabel;
    Label_VerCount: TLabel;
    Label_AuthorText: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Label_VerCountClick(Sender: TObject);
    procedure Label_YearClick(Sender: TObject);
    procedure OnClick(Sender: TObject);

  private

  public

  end;

var
  About: TAbout;
  ConfigFile, LangFile: TINIFile;
implementation

{$R *.lfm}

{ TAbout }

procedure TAbout.Label_YearClick(Sender: TObject);
begin

end;

procedure TAbout.OnClick(Sender: TObject);
begin
     About.Visible:=False;
end;

procedure TAbout.FormCreate(Sender: TObject);
begin
     ConfigFile:=TINIFile.Create('config.ini');
     LangFile:=TINIFile.Create(ConfigFile.ReadString('Paths','LangFile',''));

     About.Caption:=LangFile.ReadString('Translation','Label_About','Please, say why?');
     Label_Author.Caption:=LangFile.ReadString('Translation','Label_Author','');
     Label_Ver.Caption:=LangFile.ReadString('Translation','Label_Ver','');
     Label_ServerParser.Caption:=LangFile.ReadString('Translation','Label_ServerParser','');
end;

procedure TAbout.Label_VerCountClick(Sender: TObject);
begin

end;

end.

