unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, INIFiles, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TSettings }

  TSettings = class(TForm)
    Button_OK: TButton;
    Button_Cancel: TButton;
    Edit_RepoPath: TEdit;
    Edit_LangPath: TEdit;
    Edit_FontSize_SoftName: TEdit;
    Edit_FontSize_SoftAbout: TEdit;
    Edit_FontSize_SoftVer: TEdit;
    Edit_FontSize_ListSoftRepo: TEdit;
    Edit_FontSize_ListSoftName: TEdit;
    Edit_FontSize_Button_Install: TEdit;
    Label_LangPath: TLabel;
    Label_RepoPath: TLabel;
    Label_FontSize_SoftName: TLabel;
    Label_FontSize_SoftAbout: TLabel;
    Label_FontSize_SoftVer: TLabel;
    Label_FontSize_ListSoftRepo: TLabel;
    Label_FontSize_ListSoftName: TLabel;
    Label_FontSize_Button_Install: TLabel;
    procedure Button_CancelClick(Sender: TObject);
    procedure Button_OKClick(Sender: TObject);
    procedure Edit_FontSize_Button_InstallChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OnClose(Sender: TObject; var CloseAction: TCloseAction);
  private

  public

  end;

var
  Settings: TSettings;
  ConfigFile,LangFile: TINIFile;

implementation

{$R *.lfm}

{ TSettings }

procedure TSettings.FormCreate(Sender: TObject);
begin
     ConfigFile:=TINIFile.Create('config.ini');
     LangFile:=TINIFile.Create(ConfigFile.ReadString('Paths','LangFile',''));

     Label_FontSize_SoftName.Caption:=LangFile.ReadString('Translation','Label_FontSize_SoftName','');
     Label_FontSize_SoftAbout.Caption:=LangFile.ReadString('Translation','Label_FontSize_SoftAbout','');
     Label_FontSize_SoftVer.Caption:=LangFile.ReadString('Translation','Label_FontSize_SoftVer','');
     Label_FontSize_ListSoftRepo.Caption:=LangFile.ReadString('Translation','Label_FontSize_ListSoftRepo','');
     Label_FontSize_ListSoftName.Caption:=LangFile.ReadString('Translation','Label_FontSize_ListSoftName','');
     Label_FontSize_Button_Install.Caption:=LangFile.ReadString('Translation','Label_FontSize_Button_Install','');
     Button_OK.Caption:=LangFile.ReadString('Translation','Button_OK','');
     Button_Cancel.Caption:=LangFile.ReadString('Translation','Button_Cancel','');
     Label_RepoPath.Caption:=LangFile.ReadString('Translation','Label_RepoPath','');
     Label_LangPath.Caption:=LangFile.ReadString('Translation','Label_LangPath','');

     Edit_FontSize_SoftName.Caption:=ConfigFile.ReadString('TextSize','Label_SoftName','5');
     Edit_FontSize_SoftAbout.Caption:=ConfigFile.ReadString('TextSize','Label_SoftAbout','5');
     Edit_FontSize_SoftVer.Caption:=ConfigFile.ReadString('TextSize','Label_SoftVer','5');
     Edit_FontSize_ListSoftRepo.Caption:=ConfigFile.ReadString('TextSize','List_SoftRepo','5');
     Edit_FontSize_ListSoftName.Caption:=ConfigFile.ReadString('TextSize','List_SoftName','5');
     Edit_FontSize_Button_Install.Caption:=ConfigFile.ReadString('TextSize','Button_Install','5');
     Edit_RepoPath.Caption:=ConfigFile.ReadString('Paths','RepoFile','');
     Edit_LangPath.Caption:=ConfigFile.ReadString('Paths','LangFile','');

end;

procedure TSettings.OnClose(Sender: TObject; var CloseAction: TCloseAction);
begin

end;

procedure TSettings.Edit_FontSize_Button_InstallChange(Sender: TObject);
begin

end;

procedure TSettings.Button_OKClick(Sender: TObject);
begin
   ConfigFile.WriteString('TextSize','Label_SoftName',Edit_FontSize_SoftName.Caption);
   ConfigFile.WriteString('TextSize','Label_SoftAbout',Edit_FontSize_SoftAbout.Caption);
   ConfigFile.WriteString('TextSize','Label_SoftVer',Edit_FontSize_SoftVer.Caption);
   ConfigFile.WriteString('TextSize','List_SoftRepo',Edit_FontSize_ListSoftRepo.Caption);
   ConfigFile.WriteString('TextSize','List_SoftName',Edit_FontSize_ListSoftName.Caption);
   ConfigFile.WriteString('TextSize','Button_Install',Edit_FontSize_Button_Install.Caption);
   ConfigFile.WriteString('Paths','RepoFile',Edit_RepoPath.Caption);
   ConfigFile.WriteString('Paths','LangFile',Edit_LangPath.Caption);
   ShowMessage(LangFile.ReadString('Translation','Settings_NeedRestart_Message',''));
   Settings.Visible:=False;
end;

procedure TSettings.Button_CancelClick(Sender: TObject);
begin
  Settings.Visible:=False;
end;

end.

