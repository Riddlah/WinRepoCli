unit Unit4;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Grids, StdCtrls, ActnList, INIFiles;

type

  { TRepoListEditor }

  TRepoListEditor = class(TForm)
    Button_Remove: TButton;
    Button_Add: TButton;
    Button_Save: TButton;
    Combo_SelectRepo: TComboBox;
    RepoGrid: TStringGrid;
    procedure Button_AddClick(Sender: TObject);
    procedure Button_RemoveClick(Sender: TObject);
    procedure Button_SaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  RepoListEditor: TRepoListEditor;
    ConfigFile, LangFile, RepoFile: TINIFile;
    RepoCount:Integer;
implementation

{$R *.lfm}

{ TRepoListEditor }

procedure TRepoListEditor.FormCreate(Sender: TObject);
var
   LangFileString,RepoFileString: String;
   i:Integer;
begin
     if FileExists('config.ini') then
     begin
          ConfigFile:=TINIFile.Create('config.ini');
          LangFileString:=ConfigFile.ReadString('Paths','LangFile','');
          If(FileExists(LangFileString)) then
          begin
               LangFile:=TINIFile.Create(LangFileString);
          end else showmessage('LANG FILE NOT FOUND!');

          Button_Save.Caption:=LangFile.ReadString('Translation','Button_Save','');
          Button_Add.Caption:=LangFile.ReadString('Translation','Button_Add','');
          Button_Remove.Caption:=LangFile.ReadString('Translation','Button_Remove','');

          RepoGrid.Columns[0].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_ConfigFolder','');
          RepoGrid.Columns[1].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_ConfigFile','');
          RepoGrid.Columns[2].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_IsLocal','');
          RepoGrid.Columns[3].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_Host','');
          RepoGrid.Columns[4].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_Port','');
          RepoGrid.Columns[5].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_UserName','');
          RepoGrid.Columns[6].Title.Caption:=LangFile.ReadString('Translation','RepoListEditor_Password','');

          RepoFileString:=ConfigFile.ReadString('Paths','RepoFile','');
          if(FileExists(RepoFileString)) then
          begin
               RepoFile:=TINIFile.Create(RepoFileString);
               RepoCount:=RepoFile.ReadInteger('General','RepoCount',1);
          end else showmessage('REPOFILE NOT FOUND!');
          RepoGrid.RowCount:=RepoCount+1;
          RepoGrid.Cells[0,0]:=LangFile.ReadString('Translation','RepoListEditor_Count','')+' '+IntToStr(RepoCount);
          for i:=1 to RepoCount do
          begin
               RepoGrid.Cells[0,i]:=inttostr(i);
               RepoGrid.Cells[1,i]:=RepoFile.ReadString('Folder',inttostr(i),'');
               RepoGrid.Cells[2,i]:=RepoFile.ReadString('Path',inttostr(i),'');
               RepoGrid.Cells[3,i]:=RepoFile.ReadString('IsLocal',inttostr(i),'');
               RepoGrid.Cells[4,i]:=RepoFile.ReadString('Host',inttostr(i),'');
               RepoGrid.Cells[5,i]:=RepoFile.ReadString('Port',inttostr(i),'');
               RepoGrid.Cells[6,i]:=RepoFile.ReadString('UserName',inttostr(i),'');
               RepoGrid.Cells[7,i]:=RepoFile.ReadString('Password',inttostr(i),'');
               Combo_SelectRepo.Items.Add(IntToStr(i));
          end;
     end;
end;

procedure TRepoListEditor.Button_SaveClick(Sender: TObject);
var
   i:integer;
begin
     RepoFile.WriteInteger('General','RepoCount',RepoCount);
     for i:=1 to RepoCount do
     begin
          RepoFile.WriteString('Folder',inttostr(i),RepoGrid.Cells[1,i]);
          RepoFile.WriteString('Path',inttostr(i),RepoGrid.Cells[2,i]);
          RepoFile.WriteString('IsLocal',inttostr(i),RepoGrid.Cells[3,i]);
          RepoFile.WriteString('Host',inttostr(i),RepoGrid.Cells[4,i]);
          RepoFile.WriteString('Port',inttostr(i),RepoGrid.Cells[5,i]);
          RepoFile.WriteString('UserName',inttostr(i),RepoGrid.Cells[6,i]);
          RepoFile.WriteString('Password',inttostr(i),RepoGrid.Cells[7,i]);

     end;
end;

procedure TRepoListEditor.Button_AddClick(Sender: TObject);
begin
     RepoCount:=RepoCount+1;
     RepoGrid.RowCount:=RepoCount+1;
     Combo_SelectRepo.Items.Add(inttostr(RepoCount));
     RepoGrid.Cells[0,RepoGrid.RowCount-1]:=inttostr(RepoCount);
     RepoGrid.Cells[0,0]:=LangFile.ReadString('Translation','RepoListEditor_Count','')+' '+IntToStr(RepoCount);
end;

procedure TRepoListEditor.Button_RemoveClick(Sender: TObject);
var
   selected, i:integer;
begin
     RepoCount:=RepoCount-1;
     selected:=strtoint(Combo_SelectRepo.Caption);
     RepoGrid.DeleteRow(selected);
     Combo_SelectRepo.Items.Delete(RepoCount);
     Combo_SelectRepo.Caption:=IntToStr(RepoCount);
     for i:=selected to RepoCount do
     begin
          RepoGrid.Cells[0,i]:=inttostr(i);
     end;
     RepoGrid.Cells[0,0]:=LangFile.ReadString('Translation','RepoListEditor_Count','')+' '+IntToStr(RepoCount);
end;

end.

