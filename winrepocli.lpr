program winrepocli;

{$mode objfpc}{$H+}
{$codepage cp1252}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, Unit2, Unit3, laz_synapse, Unit4
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='WinRepoCli';
  Application.Scaled:=True;
  RequireDerivedFormResource:=True;

  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TSettings, Settings);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TRepoListEditor, RepoListEditor);
  Application.Run;
end.

